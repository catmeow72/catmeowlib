namespace CatmeowLib.OptionParser;
[System.Serializable]
public class OptionParsingException : System.Exception {
    private const string BaseErrorMessage = "An option parsing error occurred: ";
    private const string BaseErrorMessageNoMessageSpecified = "An option parsing error occurred.";
    public OptionParsingException() : base(BaseErrorMessageNoMessageSpecified) { }
    public OptionParsingException(string message) : base(BaseErrorMessage + message) { }
    public OptionParsingException(string message, System.Exception inner) : base(BaseErrorMessage + message, inner) { }
    protected OptionParsingException(
        System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
}