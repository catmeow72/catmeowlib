namespace CatmeowLib.OptionParser;
[System.Serializable]
public class InvalidOptionDescriptionException : System.Exception
{
    private const string BaseErrorMessage = "This option doesn't make sense: ";
    private const string BaseErrorMessageNoMessageSpecified = "This option doesn't make sense.";
    public InvalidOptionDescriptionException() : base(BaseErrorMessageNoMessageSpecified) { }
    public InvalidOptionDescriptionException(string message) : base(BaseErrorMessage + message) { }
    public InvalidOptionDescriptionException(string message, System.Exception inner) : base(BaseErrorMessage + message, inner) { }
    protected InvalidOptionDescriptionException(
        System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
}