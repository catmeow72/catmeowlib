using System.Reflection;
using CatmeowLib;
namespace CatmeowLib.OptionParser;
using System.Reflection;
using System;
using System.Collections.Generic;
public class OptionsParser {
    /// <Summary>
    /// A callback to get a TextWriter instance for output
    /// </Summary>
    public delegate TextWriter GetOutput();
    /// <Summary>
    /// A callback to display text at some point when the help function is called
    /// </Summary>
    public delegate void HelpCallback(TextWriter output);
    /// <Summary>
    /// A help callback to allow displaying something before the entire help message.
    /// It is recommended to use the provided TextWriter in order to use the same text as the help message.
    /// </Summary>
    public event HelpCallback? BeforeHelp;
    /// <Summary>
    /// A help callback to allow displaying something after the help message.
    /// It is recommended to use the provided TextWriter in order to use the same text as the help message.
    /// </Summary>
    public event HelpCallback? AfterHelp;
    /// <Summary>
    /// A help callback to allow displaying something after the command in the help message.
    /// It is recommended to use the provided TextWriter in order to use the same text as the help message.
    /// </Summary>
    public event HelpCallback? AfterHelpCommandUsage;
    /// <Summary>
    /// Allows providing a custom output stream for the help message, in case a custom logger that redirects stdout is in use when the options are parsed.
    /// This is not needed if nothing is redirecting the Console.Output stream and you aren't using a custom stream for output.
    /// This is a callback rather than an event because we need specifically one output stream.
    /// </Summary>
    public GetOutput? GetOutputStreamCallback;
    /// <Summary>
    /// Allows providing a custom error stream for the help message, in case a custom logger that redirects stdout is in use when the options are parsed.
    /// This is not needed if nothing is redirecting the Console.Error and you aren't using a custom stream for output.
    /// This is a callback rather than an event because we need specifically one error stream.
    /// </Summary>
    public GetOutput? GetErrorStreamCallback;

    /// <Summary>
    /// Allows the parser to know whether or not an option specified needs an argument, and if it does, what option that is
    /// </Summary>
    private OptionDescriptionBase? needNewArgumentFor = null;

    /// <Summary>
    /// Tells the parser whether the option that needs an argument is a long option or a short option
    /// Used so that the correct option is displayed when the parser detects that an option needs an argument but none is specified.
    /// </Summary>
    protected bool needNewArgumentForLong {
        get; set;
    } = false;

    /// <Summary>
    /// The list of option descriptions that this option parser uses
    /// </Summary>
    protected OptionDescriptionBase[] descriptions {
        get; set;
    }
    
    /// <Summary>
    /// Whether or not a help option was requested.
    /// </Summary>
    protected bool AddHelpOption;

    /// <Summary>
    /// The current position, excluding positions used by a non-position-based argument.
    /// </Summary>
    protected uint Position {
        get; set;
    } = 0;

    /// <Summary>
    /// Parses an argument we know is short.
    /// </Summary>
    /// <Param name="argString">The string with the argument(s) being parsed. The first character is ignored!</Param>
    protected void ParseShortOptions(string argString) {
        bool breakOutOfLoop = false;
        for (int i = 1; i < argString.Length; i++) {
            foreach (var desc in descriptions) {
                // Note that if the short option isn't null, the position must be null.
                if (desc.ShortOpt == argString[i]) {
                    if (desc.HasArgument) {
                        if (i + 1 < argString.Length) {
                            // Parse the string via reflection, since we don't know the type of the option until runtime!
                            Type DescriptionType = desc.GetType();
                            MethodBase? stringParser = DescriptionType.GetMethod("ParseString");
                            if (stringParser == null) {
                                throw new OptionParsingException("Internal error when parsing option description.");
                            }
                            stringParser.Invoke(desc, new object[] { argString.Substring(i + 1) });
                            // Make sure no more characters are parsed
                            breakOutOfLoop = true;
                            break;
                        } else {
                            needNewArgumentFor = desc;
                            needNewArgumentForLong = false;
                        }
                    } else {
                        // If it doesn't have an argument, it's type parameter must be a bool.
                        // Otherwise, we would've thrown an exception
                        ((OptionDescription<bool>)desc).OptionValue = true;
                    }
                }
            }
            if (breakOutOfLoop)
                break;
        }
    }
    private void ParseLongOption(string argString) {
        foreach (var desc in descriptions) {
            if (desc.LongOpt != null && argString == "--" + desc.LongOpt) {
                if (desc.HasArgument) {
                    needNewArgumentFor = desc;
                    needNewArgumentForLong = true;
                } else {
                    // If it doesn't have an argument, it must be a bool.
                    // Otherwise, we would've thrown an exception
                    ((OptionDescription<bool>)desc).OptionValue = true;
                }
            }
        }
    }
    private bool ParseLongOrShortOption(string argString) {
        if (argString[0] == '-') {
            if (argString[1] == '-') {
                ParseLongOption(argString);
            } else {
                ParseShortOptions(argString);
            }
            return true;
        }
        return false;
    }
    /// <Summary>
    /// Parses the options specified during class construction using user-specified arguments.
    /// </Summary>
    /// <Param name="argList">The arguments to parse</Param>
    /// <Returns>This object</Returns>
    public OptionsParser Parse(string[] argList) {
        foreach (var desc in descriptions) {
            if (!desc.HasArgument && desc.Position == null) {
                // If it doesn't have an argument, it must be a bool.
                // Otherwise, we would've thrown an exception
                ((OptionDescription<bool>)desc).OptionValue = false;
            }
        }
        Position = 0;
        needNewArgumentFor = null;
        foreach (var argString in argList) {
            if (needNewArgumentFor == null) {
                if (!ParseLongOrShortOption(argString)) {
                    bool DescriptionForThisPosition = false;
                    foreach (var desc in descriptions) {
                        if (desc.Position == Position) {
                            DescriptionForThisPosition = true;
                            // Parse the string and set the option value via reflection, since we don't know the type of the option until runtime!
                            Type DescriptionType = desc.GetType();
                            MethodInfo? stringParser = DescriptionType.GetMethod("ParseString", new Type[] { typeof(string) });
                            if (stringParser == null) {
                                throw new OptionParsingException("Internal error when parsing option description.");
                            }
                            stringParser.Invoke(desc, new object[] { argString });
                        }
                    }
                    if (!DescriptionForThisPosition) {
                        throw new OptionParsingException(string.Format("Invalid argument at position {0}: {1}", Position, argString));
                    }
                    Position += 1;
                }
            } else {
                // Parse the string and set the option value via reflection, since we don't know the type of the option until runtime!
                Type DescriptionType = needNewArgumentFor.GetType();
                MethodInfo? stringParser = DescriptionType.GetMethod("ParseString", new Type[] { typeof(string) });
                if (stringParser == null) {
                    throw new OptionParsingException("Internal error when parsing option description.");
                }
                stringParser.Invoke(needNewArgumentFor, new object[] { argString });
                // Make sure we don't attempt to parse the option again and/or throw an error
                needNewArgumentFor = null;
            }
        }
        if (needNewArgumentFor != null) {
            throw new OptionParsingException(string.Format("Option requires an argument: {0}", needNewArgumentForLong ? "--" + needNewArgumentFor.LongOpt : "-" + needNewArgumentFor.ShortOpt));
        }
        if (AddHelpOption) {
            if (DescriptionById<bool>("help").GetValue()) {
                Help(false);
                Environment.Exit(0);
            }
        }
        return this;
    }
    /// <Summary>
    /// Parses the options specified to the constructor using the command line arguments specified to the program
    /// </Summary>
    /// <Returns>This object</Returns>
    public OptionsParser Parse() {
        List<string> args = new List<string>();
        args.AddRange(Environment.GetCommandLineArgs());
        args.RemoveAt(0);
        return Parse(args.ToArray());
    }
    /// <Summary>
    /// Gets the option description that has a value of the specified type.
    /// Useful because option descriptions also contain the value of the option after argument parsing.
    /// </Summary>
    /// <Param name="option">The ID of the option</Param>
    /// <Returns>The option description with the specified ID</Returns>
    public OptionDescription<T> DescriptionById<T>(string id) {
        foreach (var i in descriptions) {
            if (i.Id == id) {
                try {
                    return (OptionDescription<T>)i;
                } catch (InvalidCastException e) {
                    throw new ArgumentException(string.Format("The option with id '{0}' doesn't have a contain the specified type.", id), e);
                }
            }
        }
        throw new ArgumentException(string.Format("The option with id '{0}' wasn't found", id));
    }
    /// <Summary>
    /// Gets the error stream to use within this class.
    /// If a callback is defined for obtaining the error stream, this function uses that.
    /// Otherwise, this function returns <see cref="Console.Error">the Console.Error stream</see>
    /// </Summary>
    /// <Returns>The error stream used by this class</Returns>
    protected TextWriter GetErrorStream() {
        if (GetErrorStreamCallback != null) {
            return GetErrorStreamCallback.Invoke();
        } else {
            return Console.Error;
        }
    }
    /// <Summary>
    /// Gets the output stream to use for this class.
    /// If a callback is defined for obtaining the output stream, this function uses that.
    /// Otherwise, this function returns <see cref="Console.Out">the Console.Out stream</see>
    /// </Summary>
    /// <Returns>The output stream used by this class</Returns>
    protected TextWriter GetOutputStream() {
        if (GetOutputStreamCallback != null) {
            return GetOutputStreamCallback.Invoke();
        } else {
            return Console.Out;
        }
    }
    /// <Summary>
    /// Shows a help message.
    /// </Summary>
    /// <Remarks>
    /// Three events are fired by this function: <see cref="CatmeowLib.OptionParser.OptionsParser.BeforeHelp">the BeforeHelp event</see>, <see cref="CatmeowLib.OptionParser.OptionsParser.AfterHelpCommandUsage">the AfterHelpCommandUsage event</see>, and <see cref="CatmeowLib.OptionParser.OptionsParser.AfterHelp">the AfterHelp event</see> (in the order they are fired).
    /// The streams used by this can be changed by setting the <see cref="CatmeowLib.OptionParser.OptionsParser.GetOutputStreamCallback">GetOutputStreamCallback callback</see> and the <see cref="CatmeowLib.OptionParser.OptionsParser.GetErrorStreamCallback">GetErrorStreamCallback callback</see>
    /// </Remarks>
    /// <Param name="error"><c>true</c> to print to the error stream, <c>false</c> to print to the output stream.</Param>
    public void Help(bool error = true) {
        TextWriter output = error ? GetErrorStream() : GetOutputStream();
        BeforeHelp?.Invoke(output);
        output.Write(Environment.CommandLine[0] + ": ");
        uint HighestPosition = 0;
        foreach (var desc in descriptions) {
            HighestPosition = Math.Max(HighestPosition, desc.Position ?? 0);
        }
        for (uint i = 0; i < HighestPosition; i++) {
            foreach (var desc in descriptions) {
                if (desc.Position == null)
                    continue;
                if (desc.Position == i) {
                    output.Write("[" + desc.Name + "] ");
                }
            }
        }
        output.WriteLine(string.Format("[OPTIONS...]"));
        AfterHelpCommandUsage?.Invoke(output);
        output.WriteLine("Options can be one or more of:");
        Queue<string[]> outputStrsList = new Queue<string[]>();
        foreach (var desc in descriptions) {
            if (desc.Position != null)
                continue;
            string[] outputStrs = new string[2];
            string outputStr1 = "";
            if (desc.LongOpt != null) {
                outputStr1 += "--" + desc.LongOpt;
                if (desc.ShortOpt != null) {
                    outputStr1 += ", ";
                }
            }
            if (desc.ShortOpt != null) {
                outputStr1 += "-" + desc.ShortOpt;
            }
            outputStrs[0] = outputStr1;
            outputStrs[1] = desc.Description;
            outputStrsList.Enqueue(outputStrs);
        }
        Queue<uint> SpacePadding = new Queue<uint>();
        uint MaxOutputSize = 0;
        foreach (var i in outputStrsList) {
            MaxOutputSize = Math.Max((uint)i[0].Length, MaxOutputSize);
        }
        foreach (var i in outputStrsList) {
            SpacePadding.Enqueue(MaxOutputSize - (uint)i[0].Length);
        }
        while (outputStrsList.TryDequeue(out var outputStrs)) {
            output.Write("  ");
            output.Write(outputStrs[0]);
            uint padding = SpacePadding.Dequeue();
            for (uint i = 0; i < padding + 2; i++) {
                output.Write(" ");
            }
            output.Write(outputStrs[1]);
            output.Write("\n");
        }
        AfterHelp?.Invoke(output);
    }
    /// <Summary>
    /// Shows the help message, and <see cref="CatmeowLib.Exit(int, object?)">sends an exit request</see> with the arguments of this function
    /// </Summary>
    /// <Remarks>
    /// See <see cref="CatmeowLib.OptionParser.OptionsParser.Help(bool)">the Help function</see> for more information about the help message
    /// If the exit code is <c>0</c>, this function sets the error parameter to <c>false</c>.
    /// Otherwise, it sets the error parameter to <c>true</c>
    /// </Remarks>
    public void ShowHelpAndExit(int exitCode = 0, object? AdditionalInformation = null) {
        Help(exitCode != 0);
        CatmeowLib.Exit(exitCode, AdditionalInformation);
    }
    /// <Summary>
    /// Shows the help message, and <see cref="CatmeowLib.ForceExit(int, object?)">sends an exit request that cannot be canceled</see> with the arguments of this function
    /// </Summary>
    /// <Remarks>
    /// See <see cref="CatmeowLib.OptionParser.OptionsParser.Help(bool)">the Help function</see> for more information about the help message
    /// If the exit code is <c>0</c>, this function sets the error parameter to <c>false</c>.
    /// Otherwise, it sets the error parameter to <c>true</c>
    /// </Remarks>
    public void ShowHelpAndForceExit(int exitCode = 0, object? AdditionalInformation = null) {
        Help(exitCode != 0);
        CatmeowLib.ForceExit(exitCode, AdditionalInformation);
    }
    /// <Summary>
    /// Creates an <see cref="OptionsParser">Options parser</see> with the specified options
    /// </Summary>
    /// <Param name="descriptions">The options to include</Param>
    /// <Param name="AddHelpOption">Whether to include an additional automatically-generated help option that shows a help message and exits after parsing the options if specified</Param>
    public OptionsParser(OptionDescriptionBase[] descriptions, bool AddHelpOption = true) {
        OptionDescriptionBase[] newDescriptions = new OptionDescriptionBase[descriptions.Length + (AddHelpOption ? 1 : 0)];
        descriptions.CopyTo(newDescriptions, 0);
        if (AddHelpOption) {
            newDescriptions[descriptions.Length /* + 1 - 1 */] = new OptionDescription<bool> {
                Id = "help",
                LongOpt = "help",
                ShortOpt = 'h',
                HasArgument = false,
                Description = "Show this message",
            };
        }
        this.descriptions = newDescriptions;
        foreach (var i in this.descriptions) {
            Type DescriptionType = i.GetType();
            MethodBase? method = DescriptionType.GetMethod("CheckValidity");
            if (method == null) {
                throw new ArgumentException("Each option description must not use the OptionDescriptionBase class directly!");
            }
            method.Invoke(i, null);
        }
        this.AddHelpOption = AddHelpOption;
    }
}