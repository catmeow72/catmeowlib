namespace CatmeowLib.OptionParser;
[System.Serializable]
public class OptionUnspecifiedException : System.Exception {
    private const string BaseErrorMessage = "WARNING: This string should go unused. This option wasn't specified (or wasn't detected): ";
    private const string BaseErrorMessageNoMessageSpecified = "This option wasn't specified (or wasn't detected).";
    public OptionUnspecifiedException() : base(BaseErrorMessageNoMessageSpecified) { }
    public OptionUnspecifiedException(string message) : base(BaseErrorMessage + message) { }
    public OptionUnspecifiedException(string message, System.Exception inner) : base(BaseErrorMessage + message, inner) { }
    protected OptionUnspecifiedException(
        System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
}