using System.Diagnostics.CodeAnalysis;
using CatmeowLib;
using static CatmeowLib.CatmeowLib;
using System.Reflection;
namespace CatmeowLib.OptionParser;
public class OptionDescriptionBase {
    /// <Summary>
    /// The ID to get the value of this argument by, or an empty string to get it directly from the option descriptions
    /// </Summary>
    public string Id { get; set; } = "";
    /// <Summary>
    /// Whether or not this option requires an argument
    /// Ignored for position-based options.
    /// </Summary>
    public bool HasArgument { get; set; } = false;
    /// <Summary>
    /// The long version of this option, or null if this option doesn't have a long version.
    /// Cannot be used with Position. HasArgument is respected.
    /// <Summary>
    public string? LongOpt { get; set; }= null;
    /// <Summary>
    /// The short version of this option, or null if this option doesn't have a short version.
    /// Cannot be used with Position. HasArgument is respected.
    /// </Summary>
    public char? ShortOpt { get; set; }= null;
    /// <Summary>
    /// For position-based options, sets the position within the argument list to use for this options.
    /// The position used when parsing does not include ones used for non-position-based options and their arguments.
    /// Set to a non-null value to make this option position-based
    /// Cannot be used with LongOpt or ShortOpt, and HasArgument is ignored.
    /// </Summary>
    public uint? Position { get; set; } = null;
    /// <Summary>
    /// For position-based options, the name of the option to show in the help message.
    /// </Summary>
    public string Name = "(No name set)";
    /// <Summary>
    /// For non-position-based options, the description to show in the help message.
    /// </Summary>
    public string Description = "(No description set)";
}
public class OptionDescription<T> : OptionDescriptionBase {
    /// <Summary>
    /// The value the option has, or either null or the type's default value.
    /// </Summary>
    internal T? OptionValue = default(T);
    public T Default {
        set {
            OptionValue = value;
        }
    }
    /// <Summary>
    /// Parses a string as an argument of this option.
    /// If it was impossible to parse the string, an InvalidCastException is thrown.
    /// Sets OptionValue.
    /// </Summary>
    /// <Param name="input">The string to parse</Param>
    /// <Exception cref="InvalidCastException">Thrown when the string can't be parsed as this option's type</Exception>
    public void ParseString(string input) {
        MethodInfo? method;
        if (typeof(T) == typeof(string)) {
            T output;
            // We know a string has a parameterless method called Clone.
            // But according to the compiler, type T may not be a string.
            // Since we just checked that it was a string at runtime, use reflection to get around a compiler error for otherwise correct code.
            method = typeof(T).GetMethod("Clone")!;
            output = (T)method.Invoke(input, null)!;
            OptionValue = output;
            return;
        }
        Type type = typeof(T);
        method = type.GetStaticMethodWithOutParams("TryParse", typeof(bool), new (Type,  bool)[] {(typeof(string), false), (type, true)});
        if (method != null) {
            object?[] methodParams = { input, null };
            bool worked = (bool)method.Invoke(null, methodParams)!;
            // Set a new variable so the compiler can be told that it is not null when IsNull returns false.
            // Otherwise, the compiler would generate a warning no matter what IsNull returns.
            object? output = null;
            output = methodParams[1];
            if (!IsNull(output) && worked) {
                OptionValue = (T)output;
                return;
            }
        }
        method = type.GetStaticMethodWithReturnValue("Parse", type, new Type[] { typeof(string) });
        if (method != null) {
            if (method.ReturnType == typeof(T)) {
                object?[] methodParams = { input };
                OptionValue = (T)method.Invoke(null, methodParams)!;
                return;
            }
        }
        throw new InvalidCastException("The string could not be parsed into the specified object! If it is not a string, it must have either a bool TryParse(string, out T) method or a T Parse(string) method.");
    }
    /// <Summary>
    /// Makes sure this option description makes sense and is valid
    /// </Summary>
    /// <Exception cref="InvalidOptionDescriptionException">Thrown when this option descriptio doesn't make sense or isn't valid.</Exception>
    public void CheckValidity() {
        if ((LongOpt != null || ShortOpt != null) && Position != null) {
            throw new InvalidOptionDescriptionException("Long and short options cannot be used with a position.");
        }
        if (LongOpt == null && ShortOpt == null && Position == null) {
            throw new InvalidOptionDescriptionException("Long and short options, and the position for a position-based argument are null, meaning this option description doesn't describe anything!");
        }
        if (!HasArgument && typeof(T) != typeof(bool) && Position == null) {
            throw new InvalidOptionDescriptionException("This option description describes an option with no argument but with a type parameter other than bool!");
        }
    }
    /// <Summary>
    /// Creates the default version of this option description.
    /// You must set the properties of this class, either by using a collection initializer, or manually.
    /// </Summary>
    public OptionDescription() {
    }
    /// <Summary>
    /// Gets the value of the option after the arguments are parsed.
    /// When an option is unspecified, an exception is thrown.
    /// </Summary>
    /// <Param name="value">The variable to set to the value of this option</Param>
    /// <Exception cref="OptionUnspecifiedException">If the value is not nullable, and the option is unspecified, this exception is thrown.</Exception>
    public void GetValue(ref T value) {
        if (!IsNull(OptionValue)) {
            value = OptionValue;
        } else {
            throw new OptionUnspecifiedException();
        }
    }
    /// <Summary>
    /// Gets the value of the option after the arguments are parsed.
    /// This version sets an existing variable to the value of the option.
    /// When an option is unspecified, the variable is set to null.
    /// </Summary>
    /// <Param name="value">The variable to set to the value of this option</Param>
    /// <Exception cref="OptionUnspecifiedException">If the value is not nullable, and the option is unspecified, this exception is thrown.</Exception>
    public void GetValueOrNull(ref T? value) {
        if (!IsNull(OptionValue)) {
            value = OptionValue;
        } else {
            value = default(T);
        }
    }
    /// <Summary>
    /// Gets the value of the option after the arguments are parsed.
    /// This version sets an existing variable to the value of the option.
    /// When an option is unspecified, an exception is thrown.
    /// </Summary>
    /// <Param name="value">The new variable to set to the value of this option</Param>
    /// <Exception cref="OptionUnspecifiedException">If the option is unspecified, this exception is thrown.</Exception>
    public void GetValueNewVar(out T value) {
        if (!IsNull(OptionValue)) {
            value = OptionValue;
        } else {
            throw new OptionUnspecifiedException();
        }
    }
    /// <Summary>
    /// Gets the value of the option after the arguments are parsed.
    /// This version sets an existing variable to the value of the option.
    /// An unspecified non-boolean option results in the variable being set to null.
    /// </Summary>
    /// <Param name="value">The new variable to set to the value of this option</Param>
    /// <Exception cref="OptionUnspecifiedException">If the value is not nullable, and the option is unspecified, this exception is thrown.</Exception>
    public void GetValueNewVarOrNull(out T? value) {
        if (!IsNull(OptionValue)) {
            value = OptionValue;
        } else {
            value = default(T);
        }
    }
    /// <Summary>
    /// Gets the value of the option after the arguments are parsed.
    /// This version returns the value of the option.
    /// If an unspecified non-boolean option is requested, an exception is thrown.
    /// </Summary>
    /// <Exception cref="OptionUnspecifiedException">If the option is unspecified, this exception is thrown.</Exception>
    public T GetValue() {
        if (!IsNull(OptionValue)) {
            return OptionValue;
        } else {
            throw new OptionUnspecifiedException();
        }
    }
    /// <Summary>
    /// Gets the value of the option after the arguments are parsed.
    /// This version returns the value of the option..
    /// If the value is nullable, an unspecified non-boolean option results in the variable being set to null.
    /// Otherwise, an exception is thrown because the default value may not be sufficient to detect when the option was unspecified.
    /// </Summary>
    public T? GetValueOrNull() {
        if (!IsNull(OptionValue)) {
            return OptionValue;
        } else {
          return default(T);
        }
    }
}