﻿using System.Reflection;
namespace CatmeowLib.EmbeddedFileLib;
public static class EmbeddedHelper
{
	private static List<Assembly> Assemblies = new List<Assembly>();
	public static void AddAssembly(Assembly assembly) {
		if (Assemblies.Contains(assembly)) {
			throw new InvalidOperationException("The assembly was already known by this library!");
		}
		Assemblies.Add(assembly);
	}
	public static void AddAssemblies(Assembly[] assemblies) {
		foreach (var assembly in assemblies) {
			AddAssembly(assembly);
		}
	}
	public static void RemoveAssembly(Assembly assembly) {
		Assemblies.Remove(assembly);
	}
	public static void RemoveAssemblies(Assembly[] assemblies) {
		foreach (var assembly in assemblies) {
			RemoveAssembly(assembly);
		}
	}
	public static string[] GetAssemblyResourcesNamespaces(Assembly assembly)
	{
		if (assembly.GetManifestResourceNames().Length == 0)
			throw new InvalidAssemblyException("Assembly must contain resources in order to detect their namespace!");
		List<string> namespaces = new List<string>();
		foreach (string file in assembly.GetManifestResourceNames())
		{
			string i = file.Split(".")[0];
			bool namespaceIncluded = false;
			foreach (string j in namespaces)
			{
				if (i == j)
					namespaceIncluded = true;
			}
			if (namespaceIncluded)
				continue;
			else
				namespaces.Add(i);
		}
		string[] ret = new string[namespaces.Count];
		for (int i = 0; i < namespaces.Count; i++)
		{
			ret[i] = namespaces[i];
		}
		return ret;
	}

	public static string GetFirstAssemblyResourceNamespace(Assembly assembly)
	{
		return GetAssemblyResourcesNamespaces(assembly)[0];
	}

	public static Stream GetEmbeddedResourceFromAssembly(Assembly assembly, string name)
	{
		string DefaultNamespace = GetFirstAssemblyResourceNamespace(assembly);
		string resourceName = DefaultNamespace + "." + name;
		Stream? resource = assembly.GetManifestResourceStream(resourceName);
		if (resource == null)
			throw new EmbeddedFileNotFoundException(string.Format("Embedded resource '{0}' was not found in namespace '{1}'!", name, DefaultNamespace));
		return resource;
	}

	public static Stream GetEmbeddedResource(string name)
	{
		foreach (var assembly in Assemblies) {
			Stream resource;
			try {
				resource = GetEmbeddedResourceFromAssembly(assembly, name);
			} catch (EmbeddedFileNotFoundException) {
				continue;
			}
			return resource;
		}
		throw new EmbeddedFileNotFoundException(string.Format("Embedded resource '{0}' was not found!", name));
	}
}
