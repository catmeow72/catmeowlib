﻿namespace CatmeowLib.EmbeddedFileLib;
[Serializable]
public class EmbeddedFileNotFoundException : Exception
{
    public EmbeddedFileNotFoundException(string message = "An embedded file was not found.") : base(message) { }
    public EmbeddedFileNotFoundException(string message, Exception inner) : base(message, inner) { }
    protected EmbeddedFileNotFoundException(
        System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
}
