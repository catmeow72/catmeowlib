using Xunit;
using System.Collections.Generic;
using static CatmeowLib.CatmeowLib;

namespace CatmeowLib.Core.Tests;

public class Tests
{
    [Fact]
    public void EscapingTest()
    {
        Assert.Equal("Q", Escape("q", new Dictionary<string, string>{
            ["q"] = "Q"
        }));
        Assert.Equal("J", Escape("q", new Dictionary<string, string>{
            ["q"] = "Q",
            ["Q"] = "J"
        }));
        Assert.Equal("Q", Escape("q", new Dictionary<string, string>{
            ["Q"] = "J",
            ["q"] = "Q"
        }));
        Assert.Equal("\\\"cats\\\"", Escape("\"cats\"", new Dictionary<string, string>{
            ["\\"] = "\\\\",
            ["\""] = "\\\""
        }));
    }
    [Fact]
    public void NullabilityTest()
    {
        Assert.True(GetNullability<int?>());
        Assert.False(GetNullability<int>());
    }
    [Fact]
    public void IsNullTest()
    {
        // Vars to test with
        int? valueTypeNull = null;
        int? valueTypeNotNull = 0;
        string? referenceTypeNull = null;
        string? referenceTypeNotNull = "";
        string NotNullableReferenceType = "";
        int NotNullableValueType = 0;

        // Things that are null
        Assert.True(IsNull(valueTypeNull));
        Assert.True(IsNull(referenceTypeNull));

        // Things that are nullable but aren't null
        Assert.False(IsNull(valueTypeNotNull));
        Assert.False(IsNull(referenceTypeNotNull));

        // Things that aren't even nullable
        Assert.False(IsNull(NotNullableValueType));
        Assert.False(IsNull(NotNullableReferenceType));
    }
}