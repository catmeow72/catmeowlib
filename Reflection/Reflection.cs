using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

namespace CatmeowLib {
    public static class Reflection {
        /// <Summary>
        /// Gets a specific static method with the specified parameters when one or more are out parameters
        /// This method uses a hacky solution to check the types (their full name).
        /// </Summary>
        /// <Param name="returns">The return value of the static method, or null when it doesn't matter</Param>
        /// <Param name="name">The name of the static method</Param>
        /// <Param name="parameters">An array of value tuples representing the types of the parameters and whether or not they are out parameters</Param>
        /// <Returns>A method info object to invoke, or null if one was not found.</Returns>
        public static MethodInfo GetStaticMethodWithOutParams(this Type type, string name, Type returns, (Type, bool)[] parameters) {
            var methods = type.GetMethods();
            foreach (var method in methods) {
                var parametersToTest = method.GetParameters();
                if (parametersToTest.Length < parameters.Length) {
                    continue;
                }
                for (var i = 0; i < parameters.Length; i++) {
                    if (parameters[i].Item2) {
                        if (parametersToTest[i].ParameterType.FullName != parameters[i].Item1.FullName + "&") {
                            continue;
                        }
                    } else {
                        if (parametersToTest[i].ParameterType != parameters[i].Item1) {
                            continue;
                        }
                    }
                }
                if (method.Name == name && method.ReturnType == (returns ?? method.ReturnType)) {
                    return method;
                }
            }
            return null;
        }
        /// <Summary>
        /// Gets a specific static method with the specified parameters and return value
        /// This method uses the <c cref="CatmeowLib.GetStaticMethodWithOutParams(Type, string, Type?, (Type, bool)[])">GetStaticMethodWithOutParams</c> function to avoid duplicating code.
        /// </Summary>
        /// <Param name="returns">The return value of the static method, or null when it doesn't matter</Param>
        /// <Param name="name">The name of the static method</Param>
        /// <Param name="parameters">An array of parameter types</Param>
        /// <Returns>A method info object to invoke, or null if one was not found.</Returns>
        public static MethodInfo GetStaticMethodWithReturnValue(this Type type, string name, Type returns, Type[] parameters) {
            (Type, bool)[] parametersToUse = new (Type, bool)[parameters.Length];
            for (int i = 0; i < parameters.Length; i++) {
                parametersToUse[i] = (parameters[i], false);
            }

            return GetStaticMethodWithOutParams(type, name, returns, parametersToUse);
        }

        /// <Summary>
        /// Gets all assemblies if the assembly argument is null, or all assemblies starting at the assembly referenced by that argument if not
        /// </Summary>
        /// <Param name="assembly">The assembly to obtain references from. If null, the entry assembly is used instead.</Param>
        /// <Param name="stopAt">The assembly in which references need to be checked for.</Param>
        /// <Returns>An array of assemblies, with the innermost assemblies first, and no duplicates.</Returns>
        public static Assembly[] GetAllAssemblies(Assembly assembly = null, Assembly stopAt = null) {
            Assembly main;
            List<Assembly> output = new List<Assembly>();
            if (assembly == null) {
                main = Assembly.GetEntryAssembly();
            } else {
                main = assembly;
            }
            foreach (var assemblyName in main.GetReferencedAssemblies()) {
                var inner = GetAllAssemblies(Assembly.Load(assemblyName));
                if (stopAt == null || inner.Contains(stopAt)) {
                    output.AddRange(inner);
                }
            }
            output.Add(main);
            for (int i = 0; i < output.Count; i++) {
                bool duplicatesRemoved = false;
                while (!duplicatesRemoved) {
                    duplicatesRemoved = true;
                    for (int j = i + 1; j < output.Count; j++) {
                        if (output[i] == output[j]) {
                            output.RemoveAt(j);
                            duplicatesRemoved = false;
                            break;
                        }
                    }
                }
            }
            return output.ToArray();
        }

        /// <Summary>
        /// Gets all types derived from this one.
        /// </Summary>
        /// <Param name="assemblies">Assemblies to search, or null to get all relevant assemblies</Param>
        /// <Returns>An array of types derived from this one.</Returns>
        public static Type[] GetDerived(this Type type, Assembly[] assemblies = null) {
            Assembly[] UsedAssemblyList;
            if (assemblies == null) {
                UsedAssemblyList = GetAllAssemblies(null, type.Assembly);
            } else {
                UsedAssemblyList = assemblies;
            }
            var output = new List<Type>();
            foreach (var assembly in UsedAssemblyList) {
                foreach (var type2 in assembly.GetTypes()) {
                    if (type2.IsSubclassOf(type)) {
                        output.Add(type2);
                    }
                }
            }
            return output.ToArray();
        }
    }
}
