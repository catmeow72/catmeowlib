using System.IO;
using System.Text;
namespace CatmeowLib.CatLogger;
public class LogWriter: TextWriter {
    public LogType logType;
    public LogWriter(LogType logType) : base() {
        this.logType = logType;
    }
    private string CurrentLine = "";
    public override Encoding Encoding => System.Text.Encoding.UTF8;
    public override void Write(char value)
    {
        if (value == '\n') {
            CatLogger.Log(logType, CurrentLine);
            CurrentLine = "";
        }
        base.Write(value);
    }
}