using System.IO;
using System;
using System.Collections.Generic;
using CatmeowLib.EmbeddedFileLib;
using System.Reflection;
using static CatmeowLib.CatmeowLib;
using static CatmeowLib.EmbeddedFileLib.EmbeddedHelper;
namespace CatmeowLib.CatLogger;
public static class CatLogger {
	/// <Summary>
	/// The settings for the logger.
	/// Use this value to retrieve the current log settings.
	/// Use SetupLogging to initially set the settings.
	/// </Summary>
	public static LogSettings logSettings {
		get; private set;
	} = new LogSettings();
	private static TextWriter Out = Console.Out;
	private static TextWriter Error = Console.Error;
	private static TextWriter? LogFileWriter;
	/// <Summary>
	/// Sets up the logging handler.
	/// Required to be called before logging.
	/// </Summary>
	/// <Param name="logSettings">The settings for the log file or null for defaults. Presets are available as static properties of the LogSettings class. You may want to modify the log file path and log file directory of the settings.</Param>
	public static void SetupLogging(LogSettings? logSettings = null) {
		CatLogger.logSettings = logSettings ?? new LogSettings();
		Console.SetOut(new LogWriter(LogType.INFO));
		Console.SetError(new LogWriter(LogType.ERROR));
		AddAssembly(Assembly.GetExecutingAssembly());
		if (CatLogger.logSettings.LogFile != "") {
			LogFileWriter = new StreamWriter(CatLogger.logSettings.LogDirectory + Path.DirectorySeparatorChar + CatLogger.logSettings.LogFile);
			LogFileWriter.WriteLine(CatLogger.logSettings.LogFilePrefix);
		}
		foreach(var i in CatLogger.logSettings.AdditionalFiles) {
			string path = Path.Join(CatLogger.logSettings.LogDirectory + Path.DirectorySeparatorChar, i.Item2);
			if (i.Item1) {
				BinaryWriter bw = new BinaryWriter(File.OpenWrite(path));
				BinaryReader br = new BinaryReader(EmbeddedHelper.GetEmbeddedResource(i.Item2));
				bw.Write(br.ReadBytes((int)br.BaseStream.Length));
				br.Close();
				bw.Close();
				br.Dispose();
				bw.Dispose();
			} else {
				File.Delete(path);
				File.Copy(i.Item2, path);
			}
		}
	}
	/// <Summary>
	/// Sets the logging settings when the log is already initialized.
	/// </Summary>
	/// <Param name="logSettingsIn">The settings for the log file or null for defaults. Presets are available as static properties of the LogSettings class. You may want to modify the log file path and log file directory of the settings.</Param>
	public static void SetLoggingSettings(LogSettings? logSettingsIn = null) {
		var logSettings = logSettingsIn ?? new LogSettings();
		if (CatLogger.logSettings.LogFile != logSettings.LogFile || CatLogger.logSettings.LogDirectory != logSettings.LogDirectory || CatLogger.logSettings.AdditionalFiles != logSettings.AdditionalFiles || CatLogger.logSettings.LogFilePrefix != logSettings.LogFilePrefix || CatLogger.logSettings.LogFileSuffix != logSettings.LogFileSuffix) {
			FinalizeLogging();
			SetupLogging(logSettings);
		} else {
			CatLogger.logSettings = logSettings;
		}
	}
	/// <Summary>
	/// Finalizes the logging by writing the ending of the log file, and closing the log file.
	/// You should always call this before exiting the application if you are using a log file.
	/// </Summary>
	public static void FinalizeLogging() {
		RemoveAssembly(Assembly.GetExecutingAssembly());
		Console.SetOut(Out);
		Console.SetError(Error);
		if (LogFileWriter != null) {
			LogFileWriter.WriteLine(logSettings.LogFileSuffix);
			LogFileWriter.Dispose();
		}
	}
	public static string? LogTypeToString(LogType type) {
		switch(type) {
			case LogType.NONE:
				return null;
			case LogType.INFO:
				return "Information";
			case LogType.WARNING:
				return "Warning";
			case LogType.ERROR:
				return "Error";
			default:
			#if DEBUG
				throw new ArgumentException("The log type used is invalid or was not added to the LogTypeToString(LogType type) function!");
			#else
				throw new ArgumentException("The log type used is invalid!");
			#endif
		}
	}
	/// <summary>
	/// Log to the console.
	/// </summary>
	/// <param name="type">The message type</param>
	/// <param name="messageArg">The message to be sent</param>
	public static void Log(LogType type, params string[] messageArg)
	{
		TextWriter tw = (type == LogType.NONE || type == LogType.INFO || logSettings.AlwaysStdout) ? Out : Error;
		string message = logSettings.LogPrefix;
		string? logTypeStr = LogTypeToString(type);
		if (logTypeStr == null) {
			logTypeStr = "";
		} else {
			message += string.Format(logSettings.PropertyFormat, Escape(logTypeStr, logSettings.escape), "Type");
		}
		if (messageArg.Length == 1) {
			message += string.Format(logSettings.MessageFormat,Escape(messageArg[messageArg.Length - 1], logSettings.messageEscape ?? logSettings.escape));
		} else if (messageArg.Length > 1) {
			for (int i = 0; i < messageArg.Length - 1; i++) {
				message += string.Format(logSettings.PropertyFormat, Escape(messageArg[i], logSettings.escape), "Property" + i.ToString());
			}
			message += string.Format(logSettings.MessageFormat,Escape(messageArg[messageArg.Length - 1], logSettings.messageEscape ?? logSettings.escape));
		}
		message += logSettings.LogSuffix;
		tw.WriteLine(message);
		if (LogFileWriter != null)
			LogFileWriter.WriteLine(message);
	}
}