namespace CatmeowLib.CatLogger;
public enum LogType
{
	NONE = 0,
	None = 0,
	INFO = 1,
	Info = 1,
	WARNING = 2,
	Warning = 2,
	ERROR = 3,
	Error = 3,
}