using System;
using System.Reflection;
using System.Collections.Generic;
namespace CatmeowLib.CatLogger;
/// <Summary>
/// The settings for the logger
/// </Summary>
public class LogSettings {
    public static LogSettings Text {
        get {
            return new LogSettings();
        }
    }
    public static LogSettings Json {
        get {
            return new LogSettings(LogPrefix: "{\n", LogSuffix: "}", PropertyFormat: "\"{1}\": \"{0}\",\n", MessageFormat: "\"Message\": \"{0}\"", escape: new Dictionary<string, string> {
                ["\\"] = "\\\\",
                ["\""] = "\\\""
            },
            LogFile: "Log.json",
            LogFilePrefix: "[\n",
            LogFileSuffix: "]");
        }
    }
    public static LogSettings Xml {
        get {
            return new LogSettings(LogPrefix: "<log-entry ", LogSuffix: "</log-entry>", PropertyFormat: "{1}=\"{0}\" ", MessageFormat: ">{0}", escape: new Dictionary<string, string> {
                ["&"] = "&amp;",
                ["<"] = "&lt;",
                [">"] = "&gt;",
                ["\""] = "&quot;"
            }, messageEscape: new Dictionary<string, string> {
                ["&"] = "&amp;",
                ["<"] = "&lt;",
                [">"] = "&gt;"
            },
            AlwaysStdout: true,
            LogFile: "Log.xml",
            LogFilePrefix: "<?xml-model href=\"LogXml.xsd\" type=\"application/xml\" schematypens=\"http://www.w3.org/2001/XMLSchema\"?>\n<?xml-stylesheet href=\"Log.css\" type=\"text/css\"?>\n<log xmlns=\"https://catserver.experimentalcraft.com/XmlNamespaces/LogXml\">",
            LogFileSuffix: "</log>",
            AdditionalFiles: new Tuple<bool, string>[]{new Tuple<bool, string>(true, "LogXml.xsd"), new Tuple<bool, string>(true, "Log.css")});
        }
    }
    public static LogSettings Html {
        get {
            return new LogSettings(LogPrefix: "<log-entry ", LogSuffix: "</log-entry>", PropertyFormat: "{1}=\"{0}\" ", MessageFormat: ">{0}", escape: new Dictionary<string, string> {
                ["&"] = "&amp;",
                ["<"] = "&lt;",
                [">"] = "&gt;",
                ["\""] = "&quot;"
            }, messageEscape: new Dictionary<string, string> {
                ["&"] = "&amp;",
                ["<"] = "&lt;",
                [">"] = "&gt;"
            },
            AlwaysStdout: true,
            LogFile: "Log.html",
            LogFilePrefix: "<!DOCTYPE html><html><head><meta charset=\"UTF-8\" /><title>Log</title><link href=\"Log.css\" type=\"text/css\" rel=\"stylesheet\"/></head><body>",
            LogFileSuffix: "</body></html>",
            AdditionalFiles: new Tuple<bool, string>[]{new Tuple<bool, string>(true, "Log.css")});
        }
    }
    /// <Summary>
    /// The prefix to add to each log entry
    /// </Summary>
    public string LogPrefix;
    /// <Summary>
    /// The suffix to add to each log entry
    /// </Summary>
    public string LogSuffix;
    /// <Summary>
    /// The format for a String.Format call done for each property. Property 0 is the value. Property 1 is the name.
    /// </Summary>
    public string PropertyFormat;
    /// <Summary>
    /// The format for a String.Format call done for the message. Property 0 is the message
    /// </Summary>
    public string MessageFormat;
    /// <Summary>
    /// A dictionary in which the keys are the values to escape and the values are the escaped values.
    /// </Summary>
    public Dictionary<string, string> escape;
    /// <Summary>
    /// Same as the escape property, except it can be null to use the escape property instead
    /// </Summary>
    public Dictionary<string, string>? messageEscape;
    /// <Summary>
    /// True to never use stderr for log output.
    /// </Summary>
    public bool AlwaysStdout;
    /// <Summary>
    /// The name of the log file.
    /// </Summary>
    public string LogDirectory;
    /// <Summary>
    /// The text to add before the beginning of the log contents in the log file
    /// </Summary>
    public string LogFile;
    /// <Summary>
    /// The text to add before the beginning of the log contents in the log file
    /// </Summary>
    public string LogFilePrefix;
    /// <Summary>
    /// The text to add before the beginning of the log contents in the log file
    /// </Summary>
    public string LogFileSuffix;
    /// <Summary>
    /// Any additional files to copy to the output directory.
    /// </Summary>
    public Tuple<bool, string>[] AdditionalFiles;
    /// <Summary>Initializes the log settings class</Summary>
    /// <Param name="LogPrefix">The prefix to add to each log entry</Param>
    /// <Param name="LogSuffix">The suffix to add to each log entry</Param>
    /// <Param name="PropertyFormat">The format for a String.Format call done for each property. Property 0 is the value. Property 1 is the name.</Param>
    /// <Param name="MessageFormat">The format for a String.Format call done for the message. Property 0 is the message</Param>
    /// <Param name="escape">A dictionary in which the keys are the values to escape and the values are the escaped values. May be null to escape nothing.</Param>
    /// <Param name="messageEscape">Same as the escape property, except it can be null to use the escape property instead</Param>
    /// <Param name="AlwaysStdout">True to never use stderr for log output.</Param>
    /// <Param name="LogFile">The name of the log file.</Param>
    /// <Param name="LogFilePrefix">The text to add before the beginning of the log contents in the log file</Param>
    /// <Param name="LogFileSuffix">The text to add before the beginning of the log contents in the log file</Param>
    /// <Param name="AdditionalFiles">Any additional files to copy to the output directory. May be null for no additional files.</Param>
    public LogSettings(string LogPrefix = "",
                       string LogSuffix = "",
                       string PropertyFormat = "[{0}] ",
                       string MessageFormat = "{0}",
                       Dictionary<string, string>? escape = null,
                       Dictionary<string, string>? messageEscape = null,
                       bool AlwaysStdout = false,
                       string LogDirectory = ".",
                       string LogFile = "",
                       string LogFilePrefix = "",
                       string LogFileSuffix = "",
                       Tuple<bool, string>[]? AdditionalFiles = null) {
        this.LogPrefix = LogPrefix;
        this.LogSuffix = LogSuffix;
        this.PropertyFormat = PropertyFormat;
        this.MessageFormat = MessageFormat;
        this.escape = escape ?? new Dictionary<string, string>();
        this.messageEscape = messageEscape;
        this.AlwaysStdout = AlwaysStdout;
        this.LogDirectory = LogDirectory;
        this.LogFile = LogFile;
        this.LogFilePrefix = LogFilePrefix;
        this.LogFileSuffix = LogFileSuffix;
        this.AdditionalFiles = AdditionalFiles ?? new Tuple<bool, string>[0];
    }
}