using System;
using System.IO;
using Xunit;
using System.Collections.Generic;
using CatmeowLib.CatLogger;
using static CatmeowLib.CatLogger.CatLogger;
using CatmeowLib;

namespace CatmeowLib.CatLogger.Tests;

public class Tests
{
    Random Rand = new Random();
    public enum LogFormat {
        TESTINGSTANDARD,
        RANDOM,
        DEFAULT,
        JSON,
        XML,
        HTML,
    }
    public string LogFormatTypeToString(LogFormat type) {
        switch (type) {
            case LogFormat.TESTINGSTANDARD:
                return "test standard";
            case LogFormat.RANDOM:
                return "random format";
            case LogFormat.DEFAULT:
                return "the default text format";
            case LogFormat.JSON:
                return "JSON";
            case LogFormat.XML:
                return "Parsable Web (XML) format";
            case LogFormat.HTML:
                return "Web (HTML) format";
            default:
                return "Unknown";
        }
    }
    private const uint NUMLOGENTRIES = 7;
    private const string LOGFILE="logtest.txt";
    private const string STANDARDLOGFILEPREFIX = "%%LOGPREFIX%%";
    private const string STANDARDLOGFILESUFFIX = "%%LOGSUFFIX%%";
    private const string STANDARDLOGENTRYPREFIX = "??PREFIX??";
    private const string STANDARDLOGENTRYSUFFIX = "??SUFFIX??";
    private const string STANDARDLOGPROPERTYFORMAT="!!{1}={0}!!";
    private const string STANDARDLOGMESSAGEFORMAT="@@{0}@@";
    private LogType[] STANDARDLOGTYPES = new LogType[] {
        LogType.INFO,
        LogType.ERROR,
        LogType.WARNING,
        LogType.NONE,
        LogType.NONE,
        LogType.INFO,
        LogType.NONE
    };
    private string[][] STANDARDLOGENTRIES = new string[][]{
        new string[] {
            "Meow",
            "Test",
        },
        new string[] {
            "Meow",
            "Test2",
        },
        new string[] {
            "Test-Animals",
            "Cat",
        },
        new string[] {
            "Test-Animals",
            "Dog",
        },
        new string[] {
            "Test-Animals",
            "Fish"
        },
        new string[] {
            "Test (Only one part)"
        },
        new string[] {
            "Test2 (Only one part)"
        }
    };
    private LogType[]? LOGTYPES;
    private string[][]? LOGENTRIES;
    private uint CHARSTOSUFFIX;
    private uint LOGFILELENGTH;
    private void CalculateLogSize(string LOGFILEPREFIX, string LOGFILESUFFIX, string LOGENTRYPREFIX, string LOGENTRYSUFFIX, string LOGPROPERTYFORMAT, string LOGMESSAGEFORMAT, LogType[] LOGTYPES, string[][] LOGENTRIES, Dictionary<string, string>? LOGESCAPE = null, Dictionary<string, string>? LOGMESSAGEESCAPE = null) {
        // Calculate characters until the suffix.
        // In order to make this more followable, add in the order the strings will be added, as much as possible.
        CHARSTOSUFFIX = (uint)LOGFILEPREFIX.Length;
        CHARSTOSUFFIX += (uint)Environment.NewLine.Length;
        for (var logIndex = 0; logIndex < NUMLOGENTRIES; logIndex++) {
            var logEntry = LOGENTRIES[logIndex];
            CHARSTOSUFFIX += (uint)LOGENTRYPREFIX.Length;
            string? logType = LogTypeToString(LOGTYPES[logIndex]);
            if (logType != null) {
                CHARSTOSUFFIX += (uint)LOGPROPERTYFORMAT.Replace("{0}", "").Replace("{1}", "").Length;
                if (LOGPROPERTYFORMAT.Contains("{1}"))
                    CHARSTOSUFFIX += (uint)("Type".Length);
                if (LOGPROPERTYFORMAT.Contains("{0}"))
                    CHARSTOSUFFIX += (uint)CatmeowLib.Escape(logType, LOGESCAPE ?? new Dictionary<string, string>()).Length;
            }
            for (var index = 0; index < logEntry.Length - 1; index++) {
                CHARSTOSUFFIX += (uint)(LOGPROPERTYFORMAT.Replace("{0}", "").Replace("{1}", "").Length);
                if (LOGPROPERTYFORMAT.Contains("{1}"))
                    CHARSTOSUFFIX += (uint)("Property" + index.ToString()).Length;
                if (LOGPROPERTYFORMAT.Contains("{0}"))
                    CHARSTOSUFFIX += (uint)(CatmeowLib.Escape(logEntry[index], LOGESCAPE ?? new Dictionary<string, string>()).Length);
            }
            CHARSTOSUFFIX += (uint)LOGMESSAGEFORMAT.Replace("{0}", "").Length;
            if (LOGMESSAGEFORMAT.Contains("{0}"))
                CHARSTOSUFFIX += (uint)(CatmeowLib.Escape(logEntry[logEntry.Length - 1], LOGMESSAGEESCAPE ?? LOGESCAPE ?? new Dictionary<string, string>()).Length);
            CHARSTOSUFFIX += (uint)LOGENTRYSUFFIX.Length;
            CHARSTOSUFFIX += (uint)Environment.NewLine.Length;
        }
        // Calculate the size of the log file based on the calculated size of the chars until the suffix added to the suffix length
        LOGFILELENGTH = CHARSTOSUFFIX;
        LOGFILELENGTH += (uint)LOGFILESUFFIX.Length;
        LOGFILELENGTH += (uint)Environment.NewLine.Length;
    }
    private LogSettings StandardLogFileTest() {
        return new LogSettings(
            LogFilePrefix: STANDARDLOGFILEPREFIX,
            LogFileSuffix: STANDARDLOGFILESUFFIX,
            LogPrefix: STANDARDLOGENTRYPREFIX,
            LogSuffix: STANDARDLOGENTRYSUFFIX,
            PropertyFormat: STANDARDLOGPROPERTYFORMAT,
            MessageFormat: STANDARDLOGMESSAGEFORMAT,
            AdditionalFiles: new Tuple<bool, string>[]{
                new Tuple<bool, string>(true, "LogXml.xsd"),
                new Tuple<bool, string>(true, "Log.css")
            },
            LogFile: LOGFILE
        );
    }
    private LogSettings SetupRandomLogFileTest() {
        string RandomEntryPrefix = Rand.RandStringASCII(Rand.RandUInt(1, 5));
        string RandomEntrySuffix = Rand.RandStringASCII(Rand.RandUInt(1, 5));
        string RandomFilePrefix = Rand.RandStringASCII(Rand.RandUInt(1, 5));
        string RandomFileSuffix = Rand.RandStringASCII(Rand.RandUInt(1, 5));
        // The formats must be at least 10 chars so that they can properly 
        string RandomPropertyFormat = Rand.RandStringASCII(Rand.RandUInt(2, /* 10 chars - 6 chars for the format specifiers */ 4)).Replace("{", "");
        string RandomMessageFormat = Rand.RandStringASCII(Rand.RandUInt(1, /* 10 chars - 3 chars for the format specifiers */ 7)).Replace("{", "");
        // Add two format specifiers randomly into the property format without the possibility of one being put inside the other.
        { 
            int splitLoc = Rand.RandInt(0, RandomPropertyFormat.Length - 1);
            string str1 = RandomPropertyFormat.Substring(0, splitLoc);
            string str2 = RandomPropertyFormat.Substring(splitLoc);
            string str3;
            string str4;
            bool isStr2 = Rand.RandByteRanged(0, 1) == 1;
            int splitLoc2;
            if (isStr2) {
                splitLoc2 = Rand.RandInt(0, str1.Length - 1);
                str3 = str1.Substring(0, splitLoc2);
                str4 = str1.Substring(splitLoc2);
                str1 = str3 + "{1}" + str4;
            } else {
                splitLoc2 = Rand.RandInt(0, str2.Length - 1);
                str3 = str2.Substring(0, splitLoc2);
                str4 = str2.Substring(splitLoc2);
                str2 = str3 + "{1}" + str4;
            }
            RandomPropertyFormat = str1 + "{0}" + str2;
        }
        int splitLoc3 = Rand.RandInt(0, RandomMessageFormat.Length - 1);
        RandomMessageFormat = RandomMessageFormat.Substring(0, splitLoc3) + "{0}" + RandomMessageFormat.Substring(splitLoc3);
        Dictionary<string, string> RandomEscapeChars = new Dictionary<string, string>();
        foreach (char i in RandomEntrySuffix) {
            if (!RandomEscapeChars.ContainsKey(i.ToString())) {
                RandomEscapeChars.Add(i.ToString(), ((char)178).ToString());
            }
        }
        foreach (char i in RandomEntryPrefix) {
            if (!RandomEscapeChars.ContainsKey(i.ToString())) {
                RandomEscapeChars.Add(i.ToString(), ((char)178).ToString());
            }
        }
        foreach (char i in RandomFileSuffix) {
            if (!RandomEscapeChars.ContainsKey(i.ToString())) {
                RandomEscapeChars.Add(i.ToString(), ((char)178).ToString());
            }
        }
        foreach (char i in RandomFilePrefix) {
            if (!RandomEscapeChars.ContainsKey(i.ToString())) {
                RandomEscapeChars.Add(i.ToString(), ((char)178).ToString());
            }
        }
        foreach (char i in RandomMessageFormat) {
            if (!RandomEscapeChars.ContainsKey(i.ToString())) {
                RandomEscapeChars.Add(i.ToString(), ((char)178).ToString());
            }
        }
        foreach (char i in RandomPropertyFormat) {
            if (!RandomEscapeChars.ContainsKey(i.ToString())) {
                RandomEscapeChars.Add(i.ToString(), ((char)178).ToString());
            }
        }

        return new LogSettings(
            LogFilePrefix: RandomFilePrefix,
            LogFileSuffix: RandomFileSuffix,
            LogPrefix: RandomEntryPrefix,
            LogSuffix: RandomEntrySuffix,
            PropertyFormat: RandomPropertyFormat,
            MessageFormat: RandomMessageFormat,
            LogFile: LOGFILE
        );
    }
    private char[] GetCharArrayFromFile(string file, int index, int count) {
        StreamReader sr = File.OpenText(logSettings.LogFile);
        char[] unused = new char[index];
        char[] buf = new char[count];
        sr.ReadBlock(unused, 0, index);
        sr.ReadBlock(buf, 0, count);
        sr.Close();
        return buf;
    }
    private string GetStringFromFile(string file, int index, int count) {
        return new string(GetCharArrayFromFile(file, index, count)).Substring(0, count);
    }
    private uint GetFileLength(string file) {
        return (uint)File.ReadAllText(file).Length;
    }
    private void CheckLogFile(LogSettings logSettingsIn, bool random, LogFormat format) {
        Console.Out.WriteLine(string.Format("Running {0} {1} log file test...", random ? "randomized" : "standard", LogFormatTypeToString(format)));
        if (random) { 
            LOGTYPES = new LogType[NUMLOGENTRIES];
            LOGENTRIES = new string[NUMLOGENTRIES][];
            for (int i = 0; i < NUMLOGENTRIES; i++) {
                uint LogEntrySize = Rand.RandUInt(1, 5);
                LOGENTRIES[i] = new string[LogEntrySize];
                // It is more realistic for the properties to be smaller than the message, so make the peoperties 5 chars max and the message 20 chars max
                for (uint j = 0; j < LogEntrySize - 1; j++) {
                    LOGENTRIES[i][j] = Rand.RandStringASCII(Rand.RandUInt(0, 5));
                }
                LOGENTRIES[i][LogEntrySize - 1] = Rand.RandStringASCII(Rand.RandUInt(0, 20));
                // Make sure to change this when adding new log types to the end of the enum.
                LOGTYPES[i] = (LogType)Rand.RandInt(0, (int)LogType.ERROR);
            }
        } else {
            LOGTYPES = STANDARDLOGTYPES;
            LOGENTRIES = STANDARDLOGENTRIES;
        }
        // Initialize the log
        LogSettings logSettings = logSettingsIn;
        logSettings.LogFile = LOGFILE;
        CalculateLogSize(logSettings.LogFilePrefix, logSettings.LogFileSuffix, logSettings.LogPrefix, logSettings.LogSuffix, logSettings.PropertyFormat, logSettings.MessageFormat, LOGTYPES, LOGENTRIES, logSettings.escape, logSettings.messageEscape);
        SetupLogging(logSettings);
        for (int i = 0; i < NUMLOGENTRIES; i++) {
            Log(LOGTYPES[i], LOGENTRIES[i]);
        }
        FinalizeLogging();
        string file = logSettings.LogFile;
        Assert.Equal(LOGFILELENGTH, GetFileLength(file));
        Assert.Equal(logSettings.LogFilePrefix, GetStringFromFile(file, 0, logSettings.LogFilePrefix.Length));
        Assert.True(CHARSTOSUFFIX + (uint)logSettings.LogFileSuffix.Length <= LOGFILELENGTH);
        Assert.Equal(logSettings.LogFileSuffix, GetStringFromFile(file, (int)CHARSTOSUFFIX, logSettings.LogFileSuffix.Length));
        uint location = (uint)logSettings.LogFilePrefix.Length + (uint)Environment.NewLine.Length;
        // 0: Log Prefix
        // 1: Log Type
        // 2: Log Property
        // 3: Log Message
        // 4: Log Suffix
        uint CurrentLogEntry = 0;
        uint CurrentDataType = 0;
        uint CurrentPropertyNum = 0;
        bool changedTo3 = false;
        while (true) {
            Assert.True(location <= LOGFILELENGTH);
            string propertyFormatted;
            switch (CurrentDataType) {
                case 0:
                    Assert.Equal(logSettings.LogPrefix, GetStringFromFile(file, (int)location, logSettings.LogPrefix.Length));
                    location += (uint)logSettings.LogPrefix.Length;
                    break;
                case 1:
                    string? logType = LogTypeToString(LOGTYPES[CurrentLogEntry]);
                    if (logType != null) {
                        propertyFormatted = string.Format(logSettings.PropertyFormat, CatmeowLib.Escape(logType, logSettings.escape), "Type");
                        Assert.Equal(propertyFormatted, GetStringFromFile(file, (int)location, propertyFormatted.Length));
                        location += (uint)propertyFormatted.Length;
                    }
                    break;
                case 2:
                    if (CurrentPropertyNum == LOGENTRIES[CurrentLogEntry].Length - 1) {
                        CurrentDataType++;
                        changedTo3 = true;
                        break;
                    }
                    propertyFormatted = string.Format(logSettings.PropertyFormat, CatmeowLib.Escape(LOGENTRIES[CurrentLogEntry][CurrentPropertyNum], logSettings.escape), "Property" + CurrentPropertyNum.ToString());
                        Assert.Equal(propertyFormatted, GetStringFromFile(file, (int)location, propertyFormatted.Length));
                    location += (uint)propertyFormatted.Length;
                    CurrentPropertyNum++;
                    break;
                case 3:
                    string messageFormat = string.Format(logSettings.MessageFormat, CatmeowLib.Escape(LOGENTRIES[CurrentLogEntry][CurrentPropertyNum], logSettings.messageEscape ?? logSettings.escape));
                    Assert.Equal(messageFormat, GetStringFromFile(file, (int)location, messageFormat.Length));
                    location += (uint)messageFormat.Length;
                    break;
                case 4:
                    Assert.Equal(logSettings.LogSuffix, GetStringFromFile(file, (int)location, logSettings.LogSuffix.Length));
                    location += (uint)logSettings.LogSuffix.Length;
                    break;
            }
            if (!changedTo3) {
                if (CurrentDataType != 2) {
                    CurrentDataType++;
                }
            } else {
                changedTo3 = false;
            }
            if (CurrentDataType == 5) {
                CurrentDataType = 0;
                CurrentPropertyNum = 0;
                CurrentLogEntry++;
                location += (uint)Environment.NewLine.Length;
            }
            if (location >= CHARSTOSUFFIX)
                break;
        }
        Console.Out.WriteLine(string.Format("Done running {0} {1} log file test...", random ? "randomized" : "standard", LogFormatTypeToString(format)));
    }

    // Standard tests
    [Fact]
    public void TestingStandardLogFileTest()
    {
        CheckLogFile(StandardLogFileTest(), false, LogFormat.TESTINGSTANDARD);
    }
    [Fact]
    public void TextLogFileTest()
    {
        CheckLogFile(LogSettings.Text, false, LogFormat.DEFAULT);
    }
    [Fact]
    public void HtmlLogFileTest()
    {
        CheckLogFile(LogSettings.Html, false, LogFormat.HTML);
    }
    [Fact]
    public void JsonLogFileTest()
    {
        CheckLogFile(LogSettings.Json, false, LogFormat.JSON);
    }
    [Fact]
    public void XmlLogFileTest()
    {
        CheckLogFile(LogSettings.Xml, false, LogFormat.XML);
    }


    // Randomized tests
    [Fact]
    public void RandomTestingStandardLogFileTest()
    {
        CheckLogFile(StandardLogFileTest(), true, LogFormat.TESTINGSTANDARD);
    }
    [Fact]
    public void RandomLogFileTest()
    {
        CheckLogFile(SetupRandomLogFileTest(), false, LogFormat.RANDOM);
    }
    [Fact]
    public void RandomRandomLogFileTest()
    {
        CheckLogFile(SetupRandomLogFileTest(), true, LogFormat.RANDOM);
    }
    [Fact]
    public void RandomTextLogFileTest()
    {
        CheckLogFile(LogSettings.Text, true, LogFormat.DEFAULT);
    }
    [Fact]
    public void RandomHtmlLogFileTest()
    {
        CheckLogFile(LogSettings.Html, true, LogFormat.HTML);
    }
    [Fact]
    public void RandomJsonLogFileTest()
    {
        CheckLogFile(LogSettings.Json, true, LogFormat.JSON);
    }
    [Fact]
    public void RandomXmlLogFileTest()
    {
        CheckLogFile(LogSettings.Xml, true, LogFormat.XML);
    }
}