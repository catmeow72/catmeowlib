# Changelog
### 0.2.6
Add a reflection helper library for netstandard2.1 (chosen for compatibility with current Godot). Contains useful functions related to reflection.
### 0.2.5
Add CryptoRandom class for using the random functions in this library with a cryptographically-secure algorithm. Useful for generating passwords.
Add exit functions for exiting and cleaning up and/or cancelling. Also add functions to show help and exit in the Option Parser.
### 0.2.4
Fix parsing of numbers by making uses of TryParse and Parse using reflection work.
Fix short options not checking the option and help message not having spaces between options and descriptions.
### 0.2.3
Fix option parsing library's internal errors, and make Parse() not include the command because that was also causing issues.
Note that short options may not always work yet.
### 0.2.2
Fix option parsing library not taking into account position-based arguments.
### 0.2.1
Add option parsing library
Remove IsNullClass because it's easier to check if it's null via `class == null`.
Make IsNull more properly detect nullability.
### 0.2.0
Same as 0.1.6 due to unfinished publish
### 0.1.6
Adds a function to check whether a value of an unknown type is null
### 0.1.5
Add a nullability checker to the core package
### 0.1.4
Make the project use the MIT license
### 0.1.3
Sets up a new project system to allow generating files for the Nuget packages
### 0.1.2
##### CatLogger
Adds new normally capitalized versions of the items to the LogType enum with the same meaning as the all-caps version
### 0.1.1
First version.
Note that it starts at 0.1.1 because a last-minute change was made to allow uploading to NuGet properly.
