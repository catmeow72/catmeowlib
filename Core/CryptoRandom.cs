using System.Security.Cryptography;
using System;
using static CatmeowLib.CatmeowLib;
namespace CatmeowLib;
public class CryptoRandom : Random {
    /// <Summary>
    /// Converts a byte array containing a raw float to a float
    /// Used within the <see cref="CatmeowLib.CryptoRandom">CryptoRandom class</see> to convert cryptographically secure random bytes to the correct type.
    /// </Summary>
    /// <Param name="array">An array. Any bytes after the 4th are ignored, and the resulting integer is generated based on the amount of bytes.</Param>
    /// <Returns>An integer</Returns>
    private Single ByteArrayToSingle(byte[] array) {
        int output = 0;
        for (int i = 0; i < array.Length && i < sizeof(int); i++) {
            output = array[i] >> (8 * i);
        }
        return output;
    }
    /// <Summary>
    /// Converts a byte array containing a raw 64-bit integer to a 64-bit integer
    /// Used within the <see cref="CatmeowLib.CryptoRandom">CryptoRandom class</see> to convert cryptographically secure random bytes to the correct type.
    /// </Summary>
    /// <Param name="array">An array. Any bytes after the 4th are ignored, and the resulting integer is generated based on the amount of bytes.</Param>
    /// <Returns>An integer</Returns>
    private Int64 ByteArrayToInt64(byte[] array) {
        Int64 output = 0;
        for (int i = 0; i < array.Length && i < sizeof(Int64); i++) {
            output = array[i] >> (8 * i);
        }
        return output;
    }
    /// <Summary>
    /// Converts a byte array containing a raw double to a double
    /// Used within the <see cref="CatmeowLib.CryptoRandom">CryptoRandom class</see> to convert cryptographically secure random bytes to the correct type.
    /// </Summary>
    /// <Param name="array">An array. Any bytes after the 4th are ignored, and the resulting integer is generated based on the amount of bytes.</Param>
    /// <Returns>An integer</Returns>
    private double ByteArrayToDouble(byte[] array) {
        double output = 0;
        for (int i = 0; i < array.Length && i < sizeof(double); i++) {
            output = array[i] >> (8 * i);
        }
        return output;
    }
    private RandomNumberGenerator random;
    public override int Next()
    {
        return RandomNumberGenerator.GetInt32(int.MaxValue);
    }
    public override int Next(int maxValue)
    {
        return RandomNumberGenerator.GetInt32(maxValue);
    }
    public override int Next(int minValue, int maxValue)
    {
        int? output = null;
        while (output == null || output < minValue) {
            Next(maxValue);
        }
        return output.Value;
    }
    public override void NextBytes(byte[] buffer)
    {
        random.GetBytes(buffer);
    }
    public override void NextBytes(Span<byte> buffer)
    {
        random.GetBytes(buffer);
    }
    public override double NextDouble()
    {
        byte[] nextValue = new byte[sizeof(double)];
        random.GetBytes(nextValue);
        return ByteArrayToDouble(nextValue);
    }
    public override long NextInt64()
    {
        byte[] nextValue = new byte[sizeof(long)];
        random.GetBytes(nextValue);
        return ByteArrayToInt64(nextValue);
    }
    public override long NextInt64(long maxValue)
    {
        long? output = null;
        while (output == null || output > maxValue) {
            NextInt64();
        }
        return output.Value;
    }
    public override long NextInt64(long minValue, long maxValue)
    {
        long? output = null;
        while (output == null || output < minValue) {
            NextInt64(maxValue);
        }
        return output.Value;
    }
    public override float NextSingle()
    {
        byte[] nextValue = new byte[sizeof(float)];
        random.GetBytes(nextValue);
        return ByteArrayToSingle(nextValue);
    }
    public CryptoRandom() : base() {
        random = RandomNumberGenerator.Create();
    }
}