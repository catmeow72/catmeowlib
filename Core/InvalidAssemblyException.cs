﻿namespace CatmeowLib;
[Serializable]
public class InvalidAssemblyException : Exception
{
    public InvalidAssemblyException(string message = "A referenced assembly has been used in a way in which it is invalid.") : base(message) { }
    public InvalidAssemblyException(string message, Exception inner) : base(message, inner) { }
    protected InvalidAssemblyException(
        System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
}
