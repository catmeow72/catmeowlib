using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;
namespace CatmeowLib;

public static class CatmeowLib {
    public static string Escape(string input, Dictionary<string, string> escape) {
        string output = input;
        foreach (var e in escape) {
            string tmp = "";
            for (var i = 0; i < output.Length - e.Key.Length + 1; i++) {
                bool shouldEscape = true;
                for (var j = 0; j < e.Key.Length; j++) {
                    if (output[i + j] != e.Key[j]) {
                        shouldEscape = false;
                        break;
                    }
                }
                if (shouldEscape) {
                    i += e.Key.Length - 1;
                    tmp += e.Value;
                    continue;
                } else {
                    tmp += output[i];
                }
            }
            output = tmp;
        }
        return output;
    }
    /// <Summary>
    /// Generates a random byte
    /// </Summary>
    /// <Param name="min">The minimum number requested</Param>
    /// <Param name="max">The maximum number requested</Param>
    public static byte RandByteRanged(this Random Rand, byte min, byte max) {
		return (byte)Rand.Next(min, max + 1);
    }
    /// <Summary>
    /// Generates a random byte
    /// </Summary>
    public static byte RandByte(this Random Rand) {
        byte[] randBytes = new byte[1];
		Rand.NextBytes(randBytes);
        return randBytes[0];
    }
    /// <Summary>
    /// Generates multiple random bytes
    /// 
    /// To use an existing array, for example for better performance, use the Rand variable in this class.
    /// It is a System.Random variable. To do this, use CatMeowLib.Rand.NextBytes(byte[] buffer).
    /// </Summary>
    /// <Param name="size">The amount of bytes in the array</Param>
    public static byte[] RandBytes(this Random Rand, uint size) {
        byte[] randBytes = new byte[size];
		Rand.NextBytes(randBytes);
        return randBytes;
    }
    /// <Summary>
    /// Generates a random integer
    /// </Summary>
    /// <Param name="min">The minimum number requested</Param>
    /// <Param name="max">The maximum number requested</Param>
	public static int RandInt(this Random Rand, int min, int max)
    {
		return Rand.Next(min, max + 1);
    }
    /// <Summary>
    /// Generates a random unsigned integer
    /// </Summary>
    /// <Param name="min">The minimum number requested</Param>
    /// <Param name="max">The maximum number requested</Param>
    public static uint RandUInt(this Random Rand, uint min, uint max) {
		return (uint)Rand.Next((int)min, (int)max + 1);
    }
    
    /// <Summary>
    /// Generates a random byte that contains a valid ASCII value that makes sense
    /// </Summary>
    public static byte RandByteASCII(this Random Rand) {
        return Rand.RandByteRanged(32, 126);
    }

    /// <Summary>
    /// Generates a random character in ASCII
    /// </Summary>
	public static char RandCharASCII(this Random Rand)
    {
		return System.Text.Encoding.ASCII.GetChars(new byte[1] {Rand.RandByteASCII()})[0];
    }
    
    /// <Summary>
    /// Generates multiple random characters in ASCII
    /// </Summary>
    /// <Param name="size">The amount of characters in the array</Param>
	public static char[] RandCharsASCII(this Random Rand, uint size)
    {
        byte[] bytes = new byte[size];
        for (int i = 0; i < size; i++) {
            bytes[i] = Rand.RandByteASCII();
        }
		return System.Text.Encoding.ASCII.GetChars(bytes);
    }
    
    /// <Summary>
    /// Generates a random string in ASCII
    /// </Summary>
    /// <Param name="size">The amount of characters in the string</Param>
	public static string RandStringASCII(this Random Rand, uint size)
    {
        byte[] bytes = new byte[size];
        for (int i = 0; i < size; i++) {
            bytes[i] = Rand.RandByteASCII();
        }
		return System.Text.Encoding.ASCII.GetString(bytes);
    }
    /// <Summary>
    /// Generates a random floating point value
    /// </Summary>
    /// <Param name="min">The minimum number requested</Param>
    /// <Param name="max">The maximum number requested</Param>
    public static float RandFloat(this Random Rand, float min, float max) {
		float randFloat = Rand.NextSingle();
		return (randFloat * (max - min)) - min;
    }
    /// <Summary>
    /// Generates a random double value
    /// </Summary>
    /// <Param name="min">The minimum number requested</Param>
    /// <Param name="max">The maximum number requested</Param>
    public static double RandDouble(this Random Rand, double min, double max) {
		double randDouble = Rand.NextDouble();
		return (randDouble * (max - min)) - min;
    }
    /// <Summary>
    /// Returns the nullability of the type accessed by reflection
    /// </Summary>
    /// <Param name="type">The type accessed via reflection</Param>
    /// <Returns>Whether the type is nullable or not</Returns>
    public static bool GetNullabilityFromReflectionType(Type type) {
        return type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>);
    }
    /// <Summary>
    /// Returns the nullability of the type accessed by reflection
    /// </Summary>
    /// <Param name="type">The type accessed via reflection</Param>
    /// <Returns>Whether the type is nullable or not</Returns>
    public static void GetNullabilityFromReflectionType(Type type, ref bool output) {
        output = GetNullabilityFromReflectionType(type);
    }
    /// <Summary>
    /// Returns the nullability of type <c>T</c>
    /// </Summary>
    /// <Returns><c>true</c> if the type is nullable, <c>false</c> otherwise</Returns>
    public static bool GetNullability<T>() {
        return GetNullabilityFromReflectionType(typeof(T));
    }
    /// <Summary>
    /// Sets a variable to the nullability of type <c>T</c>
    /// Useful when there is a generic class that detects if it's type is nullable and offers better functionality when its type is nullable since the type will not change nullability after it is set initially.
    /// </Summary>
    /// <Param name="output"><c>true</c> if the type is nullable, <c>false</c> otherwise</Returns>
    public static void GetNullability<T>(ref bool output) {
        GetNullabilityFromReflectionType(typeof(T), ref output);
    }
    /// <Summary>
    /// Determines if an object is null, no matter what type it is.
    /// If you make sure the return value of this is false, the compiler won't show a warning about a potential null value.
    /// </Summary>
    /// <Param name="input">The object to check</Param>
    public static bool IsNull([NotNullWhen(false)]object? input) {
        if (input == null) {
            return true;
        }
        if (input.GetType().IsValueType) {
            if (!GetNullabilityFromReflectionType(input.GetType())) {
                return false;
            }
            return (bool)(input.GetType().GetProperty("HasValue")!.GetValue(input)!);
        }
        return false;
    }
    /// <Summary>
    /// A function to be called whenever the program needs to clean up before exiting.
    /// An object may be passed to give any additional information about the program's state when exiting.
    /// This is used by the <see cref="CatmeowLib.OptionParser.OptionsParser.ShowHelpAndExit(int, object?)">ShowHelpAndExit</see> and <see cref="CatmeowLib.OptionParser.OptionsParser.ShowHelpAndCleanUp(int, object?)">ShowHelpAndCleanUp</see> functions.
    /// The first argument is the exit code the program intends to exit with
    /// The second argument is an object representing any additional information passed to the function.
    /// If this function returns false, the exit will be canceled.
    /// </Summary>
    public delegate bool CleanupCallback(int exitCode, object? AdditionalInformation);
    /// <Summary>
    /// Called when the a part of this library is about to exit the program.
    /// May also be called outside this library to invoke any callbacks that need to be run before the program exits.
    /// Also allows canceling an exit request from this library.
    /// See <see cref="CatmeowLib.CleanupCallback">the Cleanup Callback delegate</see> for more information about how to use this event.
    /// </Summary>
    public static event CleanupCallback? OnCleanUp;
    /// <Summary>
    /// Sends an exit request.
    /// First, fires <see cref="CatmeowLib.OnCleanUp">the OnCleanUp event</see>
    /// If the receiver of this event returns false, the program doesn't exit
    /// Otherwise, calls the Environment.Exit function with the specified exit code.
    /// <Summary>
    /// <Param name="exitCode">The exit code of the program</Param>
    /// <Param name="AdditionalInformation">Additional information to send to users of <see cref="CatmeowLib.OnCleanUp">the OnCleanUp event</see>
    /// <Remarks>See <see cref="CatmeowLib.ForceExit(int, object?)">the ForceExit function</see> for a version of this that doesn't allow the program to cancel the exit request.</Remarks>
    public static void Exit(int exitCode, object? AdditionalInformation = null) {
        if (OnCleanUp?.Invoke(exitCode, AdditionalInformation) != false) {
            Environment.Exit(exitCode);
        }
    }
    /// <Summary>
    /// Sends an exit request.
    /// First, fires <see cref="CatmeowLib.OnCleanUp">the OnCleanUp event</see>
    /// Second, calls the Environment.Exit function with the specified exit code.
    /// </Summary>
    /// <Param name="exitCode">The exit code of the program</Param>
    /// <Param name="AdditionalInformation">Additional information to send to users of <see cref="CatmeowLib.OnCleanUp">the OnCleanUp event</see>
    /// <Remarks>See <see cref="CatmeowLib.Exit(int, object?)">the Exit function</see> for a version of this that allows the program to cancel the exit request.</Remarks>
    public static void ForceExit(int exitCode, object? AdditionalInformation = null) {
        OnCleanUp?.Invoke(exitCode, AdditionalInformation);
        Environment.Exit(exitCode);
    }
}
